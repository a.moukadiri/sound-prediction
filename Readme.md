
# Features Extraction 
## To install the required libraries : 
```
pip install -r requirements.txt
```

## To generate the features for the CNN : 
```
python preprocessing_CNN.py
```

## To generate the features for the MLP : 
```
python preprocessing_MLP.py
```

This 2 commands generate a train, test data saved in the directory tools (The directory contains initialy training and testing sets)


# Model training

## To train the CNN : 
```
python CNN.py --train
```

## To train the MLP : 
```
python MLP.py --train
```

This 2 commands generate weights of the model and save it in the directory saved_models (The directory contains initialy pretrained models)


# Model evaluation 

## To evaluate the CNN : 
```
python CNN.py
```

## To evaluate the MLP : 
```
python MLP.py
```

# Soundfile prediction 

## To predict the class from a wav soudfile :
```
python predict.py --path_file "path to the file" 
```

This command return the class predicted and the sorted probabilities of classes


# Webapp 

## To run the back-end ef the Wabapp : 
```
python main.py 
```

This will open the webapp on the localhost:5000 and there you can upload a file and get the classe and the probability of the predicted classe

## To run the front-end ef the Wabapp : 

we have to acceed to the directory front using : 
```
cd front
```

And then follow the steps above : 

### Project setup
```
npm install
```
### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```







