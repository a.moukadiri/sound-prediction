import numpy as np
import argparse
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader
import torchvision
import torchvision.transforms as transforms
from torchvision.utils import make_grid
from sklearn.preprocessing import LabelEncoder

le = LabelEncoder()
le.classes_ = np.load('tools/classes.npy')


def to_categorical(y, num_classes):
    """ 1-hot encodes a tensor """
    return np.eye(num_classes)[y]



class CNN(nn.Module):
    def __init__(self):
        super(CNN, self).__init__()
        self.conv1 = nn.Conv2d(1, 16, kernel_size=2)
        self.conv2 = nn.Conv2d(16, 32, kernel_size=2)
        self.conv3 = nn.Conv2d(32, 64, kernel_size=2) 
        self.conv4 = nn.Conv2d(64, 128, kernel_size=2)         
        self.pool = nn.MaxPool2d(2)
        self.avgpool = nn.AdaptiveAvgPool2d(1)
        self.drop = nn.Dropout(0.2)
        self.out = nn.Linear(128, 10)
        self.act = nn.ReLU()
        self.soft = nn.Softmax()

    def forward(self, x):
        x = self.drop(self.pool(self.act(self.conv1(x))))
        x = self.drop(self.pool(self.act(self.conv2(x))))
        x = self.drop(self.pool(self.act(self.conv3(x))))
        x = self.drop(self.pool(self.act(self.conv4(x))))  
        x = self.avgpool(x)
        x = x.view(x.size(0), -1) # [batch_size, 28*13*13=4732]
        x = self.soft(self.out(x)) # [batch_size, 128]
        return x

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--train",required=False,action='store_true',help='file to predict',default=False)
    args = parser.parse_args()

    x_train = np.load('tools/x_train_cnn.npy',allow_pickle=True)
    y_train = np.load('tools/y_train_cnn.npy',allow_pickle=True)
    x_test = np.load('tools/x_test_cnn.npy',allow_pickle=True)
    y_test = np.load('tools/y_test_cnn.npy',allow_pickle=True)

    num_rows = 40
    num_columns = 174
    num_channels = 1

    x_train = x_train.reshape(x_train.shape[0],num_channels ,num_rows, num_columns)
    x_test = x_test.reshape(x_test.shape[0], num_channels, num_rows, num_columns)

    num_labels = 10

    def to_categorical(y, num_classes):
        """ 1-hot encodes a tensor """
        return np.eye(num_classes)[y]


    model = CNN()


    ## Specify loss and optimization functions

    # specify loss function
    criterion = nn.MSELoss()

    # specify optimizer
    optimizer = torch.optim.Adam(model.parameters())




    batch_size = 32

    x_train = torch.Tensor(x_train) # transform to torch tensor
    y_train = torch.Tensor(y_train)

    x_test = torch.Tensor(x_test) # transform to torch tensor
    y_test = torch.Tensor(y_test)

    train_dataset = torch.utils.data.TensorDataset(x_train,y_train) # create the datset
    train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=batch_size) # create the dataloader

    test_dataset = torch.utils.data.TensorDataset(x_test,y_test) # create the test datset
    test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=batch_size) # create the test dataloader


    if args.train : 

        n_epochs = 100 # training 100 epochs

        model.train() # prep model for training

        for epoch in range(n_epochs):
            # monitor training loss
            train_loss = 0.0

            ###################
            # train the model #
            ###################
            for data, target in train_loader:
                # clear the gradients of all optimized variables
                optimizer.zero_grad()
                # forward pass: compute predicted outputs by passing inputs to the model
                output = model(data)
                # calculate the loss
                loss = criterion(output, target)
                # backward pass: compute gradient of the loss with respect to model parameters
                loss.backward()
                # perform a single optimization step (parameter update)
                optimizer.step()
                # update running training loss
                train_loss += loss.item()*data.size(0)

            # print training statistics 
            # calculate average loss over an epoch
            train_loss = train_loss/len(train_loader.dataset)

            print('Epoch: {} \tTraining Loss: {:.6f}'.format(
                epoch+1, 
                train_loss
                ))
        torch.save(model.state_dict(), 'saved_models/cnn_model.pth')
    
    model.load_state_dict(torch.load('saved_models/cnn_model.pth'))


    
    # initialize lists to monitor train loss and accuracy
    train_loss = 0.0
    class_correct = list(0. for i in range(10))
    class_total = list(0. for i in range(10))

    model.eval() # prep model for *evaluation*

    for data, target in train_loader:
        # forward pass: compute predicted outputs by passing inputs to the model
        output = model(data)
        # convert output probabilities to predicted class
        _, pred = torch.max(output, 1)
        # compare predictions to true label
        _, correct = torch.max(target.data, 1)
        # calculate test accuracy for each object class
        for i in range(len(pred)):
            label = correct[i]
            class_correct[label] += (pred[i] == label).item()
            class_total[label] += 1


    for i in range(10):
        if class_total[i] > 0:
            print('Train Accuracy of %5s: %2d%% (%2d/%2d)' % (
                le.inverse_transform([i])[0], 100 * class_correct[i] / class_total[i],
                np.sum(class_correct[i]), np.sum(class_total[i])))

    print('\nTrain Accuracy (Overall): %2d%% (%2d/%2d)\n' % (
        100. * np.sum(class_correct) / np.sum(class_total),
        np.sum(class_correct), np.sum(class_total)))        




    # initialize lists to monitor test loss and accuracy
    test_loss = 0.0
    class_correct = list(0. for i in range(10))
    class_total = list(0. for i in range(10))

    model.eval() # prep model for *evaluation*

    for data, target in test_loader:
        # forward pass: compute predicted outputs by passing inputs to the model
        output = model(data)
        # convert output probabilities to predicted class
        _, pred = torch.max(output, 1)
        # compare predictions to true label
        _, correct = torch.max(target.data, 1)
        # calculate test accuracy for each object class
        for i in range(len(pred)):
            label = correct[i]
            class_correct[label] += (pred[i] == label).item()
            class_total[label] += 1

    for i in range(10):
        if class_total[i] > 0:
            print('Test Accuracy of %5s: %2d%% (%2d/%2d)' % (
                le.inverse_transform([i])[0], 100 * class_correct[i] / class_total[i],
                np.sum(class_correct[i]), np.sum(class_total[i])))
        else:
            print('Test Accuracy of %5s: N/A (no training examples)' % (classes[i]))

    print('\nTest Accuracy (Overall): %2d%% (%2d/%2d)\n' % (
        100. * np.sum(class_correct) / np.sum(class_total),
        np.sum(class_correct), np.sum(class_total)))            


