import tensorflow as tf
from tensorflow import keras
from sklearn import metrics
from sklearn.preprocessing import LabelEncoder
import librosa
import numpy as np
import pandas as pd
import os
import urllib.request
from app import app
from flask import Flask, flash, request, redirect, render_template
from werkzeug.utils import secure_filename
from flask_cors import CORS
from flask import jsonify
from predict import print_prediction

ALLOWED_EXTENSIONS = set(['wav', 'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/')
def upload_form():
    return render_template('upload.html')


@app.route('/', methods=['POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            json = jsonify(message='No file part')
            return json, 400
        file = request.files['file']
        if file.filename == '':
            json = jsonify(message='No file selected for uploading')
            return json, 400
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))


            # prediction
            pred, out = print_prediction("fichier uploaded test2/" + filename)
            proba = str(out[0][1]) # probability for the most confident class
            json = jsonify(prediction=pred, proba=proba, message='File successfully uploaded')
            return json, 200
        else:
            json = jsonify(message='')
            return json, 400


if __name__ == "__main__":
    CORS(app)
    app.run(port=5000)
