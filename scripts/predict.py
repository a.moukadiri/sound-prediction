from CNN import CNN
from preprocessing_cnn import extract_features
from sklearn.preprocessing import LabelEncoder
import numpy as np
import argparse
import torch




le = LabelEncoder()
le.classes_ = np.load('tools/classes.npy')


num_rows = 40
num_columns = 174
num_channels = 1

model = CNN()
model.load_state_dict(torch.load('saved_models/cnn_model.pth'))


def print_prediction(file_name):
    prediction_feature = extract_features(file_name) 
    prediction_feature = torch.tensor(prediction_feature.reshape(1,num_channels, num_rows, num_columns))
    output = model(prediction_feature)
    predicted_proba,predicted_class = torch.max(output, 1)
    predicted_class = le.inverse_transform([predicted_class.item()]) 
    print("The predicted class is:", predicted_class[0], '\n')
    dict_f = {le.inverse_transform([i])[0]:j for i,j in enumerate(np.asarray(output[0].detach()))}
    output_ = [(k+'       :     ',v) for k, v in sorted(dict_f.items(), key=lambda item: item[1],reverse=True)]
    for i in range(10): 
        category = le.inverse_transform([i])
        print(category[0]+ "\t\t : "+   "%e"%(output[0][i].item()*100))
    return predicted_class[0],output_



if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--path_file",required=True,help='file to predict')
    args = parser.parse_args()
    print_prediction(args.path_file)
    